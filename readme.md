## CVAR Internal Wiki

### Local configuration

Add a known host in order to be easily accesible (in */etc/hosts*):

`192.168.0.136    wiki-cvar`

Open your favorite browser, introduce `wiki-cvar/` and press enter. Now, you you can login with your Github account and start reading our amazing Wiki!

**Note: If you require WRITE access, please contact to** *alejandro.rramos@upm.es*

### Create a folder in the Wiki

In order to create a folder where you can include pages from the **same topic** you can easily press the *create* button and simply add `my_folder/my_new_page` as the page name. This will automatically create the new folder and page.

### Remote configuration

To be done soon..