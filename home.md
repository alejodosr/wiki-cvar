<!-- TITLE: CVAR Internal Wiki -->
<!-- SUBTITLE: Stand on the shoulders of giants -->

# CVAR Internal Wiki
The wiki is organized in pages, such as aerostack, machine learning, etc. Everything can be written in MarkDown code (https://es.wikipedia.org/wiki/Markdown) Feel free to edit whatever you require, but always thinking in the community! Enjoy!