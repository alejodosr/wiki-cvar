<!-- TITLE: Install Qtcreator In Ubuntu 16 04 -->
<!-- SUBTITLE: A quick summary of Install Qtcreator In Ubuntu 16 04 -->


The default qtcreator installed from apt-get freezes in ubuntu 16.04, hence a different version needs to be installed. It can be done following these steps:
1. Download the qt file for linux:
http://download.qt.io/official_releases/qt/5.6/5.6.2/
2. Install this downloaded Qt in the desired location preferably not in root.
3. After installation launch qtcreator and enter the following location
Tool>options>Kits
4. Select the auto detected kit and in the cmake generator select <use default generator>
5. and then Run cmake, and everything should be running good.

Use the following commands to create a symbolic link to qt
1. sudo rm -f /usr/local/bin/qtcreator 
2. sudo ln -s $QTC_BUILD/bin/qtcreator /usr/local/bin/qtcreator

NOTE:
This solution doesn't work perfectly: 
Its better to download qtcreator 3.6, one version higher than the qtcreator 3.5 provided default by ubuntu. 
It can be downloaded from the same link above. Install and make a symbolic link and qtcreator will be up and running.