<!-- TITLE: Using Grep With Ubuntu -->
<!-- SUBTITLE: A quick summary of Using Grep With Ubuntu -->

1. Find a word in a directory using ubuntu grep

`grep -r "sensorMsgsImageToCVMat" /usr/opt/ros/kinectic`

2. Find a process (gazebo) running in ubuntu

`ps ax | grep gazebo`