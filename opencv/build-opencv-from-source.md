<!-- TITLE: Disable Suspend Or Hibernation In Ubuntu -->
Follow the instructions in the link below:
https://stackoverflow.com/questions/35825391/opencv-make-on-ubuntu-with-opencv-contrib-fails-due-to-opencv-xphoto

When you use opencv with opencv_contrib, make sure that both of them have the same release version. For example if opencv version is 3.3.1 then, opencv_contrib should have the same version 3.3.1. This can be easily checked in github repos of both packages.

Dont follows belows steps 1 and 2, just make install opencv in your local directory and put that one is your bashrc.
And later and into the cmake list the following:
1. `set(OpenCV_DIR /home/carrio/opencv-3.1.0/build)`
2. `find_package( OpenCV REQUIRED PATHS /home/carrio/opencv-3.1.0/cmake)`

When using ros and using use this package of cv_bridge in your workspace
https://github.com/mikejmills/vision_opencv

When installing opencv with extra modules if the download of the modules fails because of curl do the following
https://github.com/opencv/opencv_contrib/issues/1131

