This document is when writing a mat file using c++ code and using opencv as well. 

Follow the steps below:
1. `In your cmake include link_directories(/usr/local/MATLAB/R2017a/bin/glnxa64)`
2. `target_link_libraries(${PROJECT_NAME}_node libmat.so libmx.so)`
3. `target_link_libraries(${PROJECT_NAME}_node /usr/lib/x86_64-linux-gnu/libtiff.so.5)` - This is needed because opencv gives an error of libtiff

If you also get an error regarding qt xcb lib when executing the node then do the following: 
1. `In a terminal do export QT_DEBUG_PLUGINS=1`
2. `In the same terminal excute your node and qt will print the direction its looking for the libxcb`
3. `Once you have the path copy /usr/lib/x86_64-linux-gnu/qt5/plugins/platforms/ to the path the qt is looking for libxcb`

This is a temporary fix with qt it can be improved further. 

