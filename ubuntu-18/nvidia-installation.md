# Nvidia driver installation in Ubuntu 18.04
- Purge drivers
`sudo apt-get purge nvidia-*`

- Go to software & updates and select nvidia drivers

- Check if you are using the correct kernel nvidia (and not nouveau)
`lspci -k`

- If not using nvidia, disable wayland
- Check if using wayland
`loginctl show-session c1 -p Type`

- Edit `/etc/gdm3/custom.conf` and uncomment `WaylandEnable=false`

- Blacklist nouveau driver
`sudo bash -c "echo blacklist nouveau > /etc/modprobe.d/blacklist-nvidia-nouveau.conf"`
`sudo bash -c "echo options nouveau modeset=0 >> /etc/modprobe.d/blacklist-nvidia-nouveau.conf"`

- Make sure there is no other file in `/etc/modprobe.d/` which blacklists nvidia drivers

- Regenerate initramfs
`sudo update-initramfs -u`

- Reboot your system
`sudo reboot`