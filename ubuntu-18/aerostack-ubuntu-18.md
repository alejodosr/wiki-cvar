<!-- TITLE: Aerostack Ubuntu 18 -->
<!-- SUBTITLE: A quick installation summary of Aerostack in Ubuntu 18 -->

# Aerostack installation in Ubuntu 18

This is not a full installation guide of Aerostack, it is only a short guide of the differences with respect to previous Ubuntu versions.

## Cmake issues

**audio_common:**
CATKIN_IGNORE

**opencv_apps:** 
`git clone https://github.com/ros-perception/opencv_apps $AEROSTACK_STACK/stack_devel/opencv_apps`

**Qt5Svg.cmake:** 
`sudo apt-get install git build-essential cmake qt5-default qtscript5-dev libssl-dev qttools5-dev qttools5-dev-tools qtmultimedia5-dev libqt5svg5-dev libqt5webkit5-dev libsdl2-dev libasound2 libxmu-dev libxi-dev freeglut3-dev libasound2-dev libjack-jackd2-dev libxrandr-dev libqt5xmlpatterns5-dev libqt5xmlpatterns5`

**joy:** 
`git clone https://github.com/ros-drivers/joystick_drivers $AEROSTACK_STACK/stack_devel/joystick_drivers`

**libusb:** 
`sudo apt-get install libusb-dev`

**octomap_rosConfig.cmake:**
`git clone https://github.com/OctoMap/octomap_ros $AEROSTACK_STACK/stack_devel/octomap_ros`

## Building issues
**ModuleNotFoundError: No module named rospkg:**
python should be linking at python 2.7. If you are using Conda, make sure to comment "export PATH="/home/username/anaconda3/bin:$PATH"" in your .bashrc

**logInform, logError, logDebug, logWarn:**
`cd $AEROSTACK_STACK`
`find ./ -type f -readable -writable -exec sed -i "s/logError/CONSOLE_BRIDGE_logError/g" {} \;`
`find ./ -type f -readable -writable -exec sed -i "s/logInform/CONSOLE_BRIDGE_logInform/g" {} \;`
`find ./ -type f -readable -writable -exec sed -i "s/logDebug/CONSOLE_BRIDGE_logDebug/g" {} \;`
`find ./ -type f -readable -writable -exec sed -i "s/logWarn/CONSOLE_BRIDGE_logWarn/g" {} \;`

**spnav.h**:
`sudo apt-get install libspnav-dev`

**parrot_arsdk**
update to the latest commit and include `add_definitions (-std=c++11)` in CMakeLists.txt

**usb_cam**:
CATKIN_IGNORE

**rosserial:**
CATKIN_IGNORE

**asctec_autopilot:** 
CATKIN_IGNORE

**bluetooth.h:** 
`sudo apt-get install libbluetooth-dev`

**cwiid.h:** 
`git clone https://github.com/abstrakraft/cwiid $AEROSTACK_STACK/stack_devel/cwiid`
`sudo apt-get install libcwiid-dev`

**ardrone_autonomy:**
CATKIN_IGNORE

**/home/alejandro/workspace/ros/aerostack_catkin_ws/src/aerostack_stack/stack/ground_control_system/rviz/droneArchitectureRvizInterfaceROSModule/CMakeLists.txt:** 
`add_definitions(-std=c++11)`

**ARCONTROLLER_DICTIONARY_KEY_COMMON_MAVLINKSTATE_MISSONITEMEXECUTED:**
(correct typo) ‘ARCONTROLLER_DICTIONARY_KEY_COMMON_MAVLINKSTATE_MISSONITEMEXECUTED’ -> ‘ARCONTROLLER_DICTIONARY_KEY_COMMON_MAVLINKSTATE_MISSIONITEMEXECUTED’
and
‘ARCONTROLLER_DICTIONARY_KEY_COMMON_MAVLINKSTATE_MISSONITEMEXECUTED_IDX' -> ‘ARCONTROLLER_DICTIONARY_KEY_COMMON_MAVLINKSTATE_MISSIONITEMEXECUTED_IDX" [https://github.com/AutonomyLab/bebop_autonomy/issues/126]

**asctec_msgsConfig.cmake:**
CATKIN_IGNORE

**libARCommands.h :** 
git clone https://github.com/Parrot-Developers/libARCommands $AEROSTACK_STACK/stack_devel/libARCommands

**dji:** 
CATKIN_IGNORE

**IMPORTANT ISSUE:**
`${GAZEBO_LIBRARIES}` have a format in Gazebo 9 that conflicts with cmake standards.
First, overwrite with the last stable version of Gazebo `cd $AEROSTACK_STACK/stack/simulation_system/drone_simulator/rotors_simulator_gazebo` and then `git pull origin stable/gazebo9`
Include in `$AEROSTACK_STACK/stack/simulation_system/drone_simulator/rotors_simulator_gazebo/CMakeLists.txt` (just before the first time `${GAZEBO_LIBRARIES}` appear):

`set(GAZEBO_LIBRARIES "")`

`find_library(GAZEBO_LIBRARIES`
       ` NAMES`
                `libgazebo.so`
                `libgazebo_client.so`
                `libgazebo_gui.so`
                `libgazebo_sensors.so`
                `libgazebo_rendering.so`
                `libgazebo_physics.so`
                `libgazebo_ode.so`
                `libgazebo_transport.so`
                `libgazebo_msgs.so`
                `libgazebo_util.so`
                `libgazebo_common.so`
                `libgazebo_gimpact.so`
                `libgazebo_opcode.so`
                `libgazebo_opende_ou.so`
                `libgazebo_math.so`
                `libgazebo_ccd.so`
                `libboost_thread.so`
                `libboost_signals.so`
                `libboost_system.so`
                `libboost_filesystem.so`
                `libboost_program_options.so`
                `libboost_regex.so`
                `libboost_iostreams.so`
                `libboost_date_time.so`
                `libboost_chrono.so`
                `libboost_atomic.so`
                `libpthread.so`
                `libprotobuf.so`
                `libprotobuf.so`
                `libpthread.so`
                `libsdformat.so`
                `libignition-math2.so`
                `libOgreMain.so`
                `libOgreMain.so`
                `libboost_thread.so`
                `libboost_date_time.so`
                `libboost_system.so`
                `libboost_atomic.so`
                `libboost_chrono.so`
                `libpthread.so`
                `libOgreTerrain.so`
                `libOgreTerrain.so`
                `libOgrePaging.so`
                `libOgrePaging.so`
                `libignition-math2.so`
        `PATHS`
               ` /usr/lib/x86_64-linux-gnu`
`)`

## Bebop Autonomy Issues
Update both bebop_autonomy and parrot_arsdk to the latest commit (on indigo-devel branch)

Include this in .bashrc
`export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$AEROSTACK_WORKSPACE/devel/lib/parrot_arsdk`


