<!-- TITLE: Difference Between Euler Rates And Angular Velocities -->
<!-- SUBTITLE: A quick summary of Difference Between Euler Rates And Angular Velocities -->


The main difference between Euler rates and Angular velocities is that,

1. Euler rates are measured in world co-ordinates, because Euler angles are measured in world coordinates.
2. Angular velocities are measured in robot coordinates.

In order to convert Euler rates to Angular velocities, we need to convert Euler rates to robot coordinates. This is done in Altitude filtering paper in equation number 8.