<!-- TITLE: Euler Angles -->
<!-- SUBTITLE: A quick summary of Euler Angles -->


Euler angles are generally divided into two types:
1. Classic Euler Angles.
2. Tait–Bryan angles

There are 12 kinds of euler angles. In aviation and aerospace industry the euler anlges used are z-y’-x″ and hence the rotation angles obtained are based on these euler angles. These are the intrinsic angles and hence are called yaw, pitch and roll.

See this link : https://en.wikipedia.org/wiki/Euler_angles
Refer to this book for rotation angles:
Biever_2015.pdf