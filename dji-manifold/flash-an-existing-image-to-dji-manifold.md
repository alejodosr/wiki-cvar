<!-- TITLE: Flash An Existing Image To Dji Manifold -->
<!-- SUBTITLE: A quick summary of Flash An Existing Image To Dji Manifold -->

# Flash an existing backup image to the dji manifold
Requirements: Ubuntu 14.04
First refer the tutorial of creating a backup image of manifold in order to download the required files.

Use the previosuly generated system.img and insert it into the bootloader folder. And use the following command to backup from the previous image

sudo ./flash.sh -r -S 14580MiB jetson-tk1 mmcblk0p1

-r means use the existing system image and not generate a new one. 

There is a backup image of the manifold in the red computer of acer, containing the data of the manifold used to fly the mission of the calder in Meirama. Ask Hriday for more details. 