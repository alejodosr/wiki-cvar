<!-- TITLE: Create System Image -->
<!-- SUBTITLE: A quick summary of Create System Image -->

# How to Create a System image of DJI manifold for backup 
Requirements: ubuntu 14.04

Download the image creator (Manifold Image Package - manifold_image_v1.0.tar.gz) for DJI manifold from here https://www.dji.com/es/manifold/info#downloads
 Dji manifold is Nvidia tegra tx1 so the instructions are more or less similar. 
 
 Extract the image using below command. (NOTE: for extracting the image use the below command exactly otherwise it will not work)
 - sudo tar -xvpzf <your path>/manifold_image_v1.0.tar.gz
 
 Use the command to create a backup of the image:

- sudo ./nvflash --read APP system.img --bl ardbeg/fastboot.bin --go