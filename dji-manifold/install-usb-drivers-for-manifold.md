<!-- TITLE: Install Usb Drivers For Manifold -->
<!-- SUBTITLE: A quick summary of Install Usb Drivers For Manifold -->

# How to Install USB drivers for DJI Manifold

Easy workaround:
The link below installs a different kernel for Manifold, it easy to install but the camera probably wont work with this kernel. 
http://meixinhu.com/2017/10/14/dji-manifold-recovery-and-setup/

Time consuming but working:

1. First install the kernel for manifold from https://www.dji.com/es/manifold/info#downloads (Manifold Kernel Source Package (manifold_kernel_source_v1.0.tar.gz))
2. Second copy the config file for manifold config into .config using this command : cp arch/arm/configs/manifold_config .config
3. make menuconfig (In this step you have to enable the USB FTDI driver, as shown at the end of this video https://www.youtube.com/watch?v=OU_yBt5NSsI)
4. make 

Once the kernel modules are built follow the following commands to copy them to the system
1. make modules_install 
2. sudo cp /boot/zImage /boot/zImage.bak //backup the zImage
3. sudo cp arch/arm/boot/zImage /boot/ //cp zImage
4. sudo cp arch/arm/boot/dts/tegra124-jetson_tk1-pm375-000-c00-00.dt*  /boot/ //copy device tree



