<!-- TITLE: Adjust Camera Speed -->
<!-- SUBTITLE: A quick tutorial to change the camera speed -->

# Create a custom pawn following the link
https://answers.unrealengine.com/questions/20136/adjust-movement-speed-of-default-camera-on-blank-p.html?sort=oldest

If not working even after performing previous operations:

* Click "Play" and then "Shift + F11"
* Search for "Pawn" component
* Edit the "DefaultPawn" component and modify its speed settings