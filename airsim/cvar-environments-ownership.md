<!-- TITLE: CVAR Environments Ownership -->
<!-- SUBTITLE: A quick summary of Cvar Environments Ownership -->

# WORTH CHECKING
Office medley - https://www.unrealengine.com/marketplace/office-medley
Modular Warehouse - https://www.unrealengine.com/marketplace/modular-warehouse
Industrial Building - https://www.unrealengine.com/marketplace/industrial-building
Modular Alley - https://www.unrealengine.com/marketplace/modular-alley
Modular Urban Exterior Building Kit - https://www.unrealengine.com/marketplace/modular-urban-exterior-building-kit
DevTon Spring Landscape - https://www.unrealengine.com/marketplace/devton-spring-landscape
Stone Pack5 - https://www.unrealengine.com/marketplace/stone-pack5
Russian Winter Town - https://www.unrealengine.com/marketplace/russian-winter-town
Victorian Street - https://www.unrealengine.com/marketplace/victorian-street
Industrial Area Hangar - https://www.unrealengine.com/marketplace/industrial-area-hangar
Industrial City - https://www.unrealengine.com/marketplace/industrial-city
Facades - https://www.unrealengine.com/marketplace/facades
Urban City - https://www.unrealengine.com/marketplace/urban-city
GR Realistic Female 01 - https://www.unrealengine.com/marketplace/gr-customizable-realistic-female
Simple Streets - https://www.unrealengine.com/marketplace/simple-streets
Modular Warehouse - https://www.unrealengine.com/marketplace/modular-warehouse
QA Modular Parking - https://www.unrealengine.com/marketplace/qa-modular-parking
DoN's 3D Pathfinding for Flying AI - https://www.unrealengine.com/marketplace/don-s-3d-pathfinding-flying-ai [Plugin for path finding]
Procedural Landscape Ecosystem - https://www.unrealengine.com/marketplace/procedural-landscape-ecosystem [Season roads!]

# ALREADY PURCHASED
Modular Neighborhood Pack - https://www.unrealengine.com/marketplace/modular-neighborhood-pack
GR Male Hero 02 - https://www.unrealengine.com/marketplace/gr-male-hero-01
Driveable Cars Basic Pack (3d assets) - https://www.unrealengine.com/marketplace/driveable-cars-basic-pack-3d-assets
DownTown - https://www.unrealengine.com/marketplace/downtown
City Alley Props - https://www.unrealengine.com/marketplace/city-alley-props